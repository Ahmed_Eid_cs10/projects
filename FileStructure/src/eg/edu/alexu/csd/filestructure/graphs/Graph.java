package eg.edu.alexu.csd.filestructure.graphs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.PriorityQueue;

import javax.management.RuntimeErrorException;

public class Graph implements IGraph {
	private boolean negativeWeight = false;
	private ArrayList<Integer> node = new ArrayList<Integer>();
	private ArrayList<ArrayList<Pair>> adjacencyList = new ArrayList<ArrayList<Pair>>();
	private ArrayList<Integer> DijkstraOrder = new ArrayList<Integer>();
	private ArrayList<ArrayList<Integer>> neighbours = new ArrayList<ArrayList<Integer>>();
	private PriorityQueue<Node> priorityQueue;
	private HashSet<Integer> set;
	private int V, E, from, to, weight;

	@Override
	public void readGraph(File file) {

		BufferedReader br = null;

		try {

			String line;
			br = new BufferedReader(new FileReader(file));
			line = br.readLine();
			if (line != null) {
				String[] split = line.split("\\s+");

				if (split.length != 2) {
					throw new RuntimeException();
				} else {

					try {
						V = Integer.parseInt(split[0]);
						E = Integer.parseInt(split[1]);
						if (V <= 0) {
							throw new RuntimeException();
						} else {
							for (int i = 0; i < V; i++) {
								adjacencyList.add(new ArrayList<Pair>());
								neighbours.add(new ArrayList<Integer>());
							}
						}
					} catch (Exception e) {
						throw new RuntimeErrorException(null);
					}
				}
			} else {
				throw new RuntimeException();
			}
			int i;
			for (i = 0; (line = br.readLine()) != null && i < E; i++) {

				String[] splited = line.split("\\s+");
				if (splited.length != 3) {
					throw new RuntimeException();
				} else {
					try {
						from = Integer.parseInt(splited[0]);
						to = Integer.parseInt(splited[1]);
						weight = Integer.parseInt(splited[2]);
					} catch (Exception e) {
						throw new RuntimeErrorException(null);
					}
					if (weight < 0) {
						negativeWeight = true;
					}

					ArrayList<Pair> adjacent = adjacencyList.get(from);
					adjacent.add(new Pair(to, weight));
					ArrayList<Integer> neighbourFrom = neighbours.get(from);
					if (!neighbourFrom.contains(to)) {
						neighbourFrom.add(to);
					}

					if (!node.contains(from)) {
						node.add(from);
					}

					if (!node.contains(to)) {

						node.add(to);
					}
				}
			}
			if (i != E || line != null) {
				throw new RuntimeException();
			}

		} catch (IOException e) {
			throw new RuntimeErrorException(null);

		} finally {

			try {

				if (br != null) {
					br.close();
				}

			} catch (IOException ex) {
				throw new RuntimeException();

			}
		}

	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return E;
	}

	@Override
	public ArrayList<Integer> getVertices() {
		// TODO Auto-generated method stub
		return node;
	}

	@Override
	public ArrayList<Integer> getNeighbors(int v) {
		// TODO Auto-generated method stub
		return this.neighbours.get(v);
	}

	@Override
	public void runDijkstra(int src, int[] distances) {
		// TODO Auto-generated method stub
		if (!negativeWeight) {
			set = new HashSet<Integer>();
			priorityQueue = new PriorityQueue<Node>(V, new Node());
			int evaluationNode;
			for (int i = 0; i < V; i++) {
				distances[i] = Integer.MAX_VALUE / 2;
			}
			priorityQueue.add(new Node(src, 0));
			distances[src] = 0;
			while (!priorityQueue.isEmpty()) {
				Node temp = priorityQueue.remove();
				DijkstraOrder.add(temp.node);
				evaluationNode = temp.node;
				set.add(evaluationNode);
				int edgeDistance = -1,newDistance=-1;
				for (int destinationNode = 0; destinationNode < adjacencyList.get(evaluationNode)
						.size(); destinationNode++) {
					if (!set.contains(adjacencyList.get(evaluationNode).get(destinationNode).first)) {
						edgeDistance = adjacencyList.get(evaluationNode).get(destinationNode).second;
						newDistance = distances[evaluationNode] + edgeDistance;
						if (newDistance < distances[adjacencyList.get(evaluationNode).get(destinationNode).first]) {
							distances[adjacencyList.get(evaluationNode).get(destinationNode).first] = newDistance;
							priorityQueue.add(new Node(adjacencyList.get(evaluationNode).get(destinationNode).first,
									distances[adjacencyList.get(evaluationNode).get(destinationNode).first]));

						}
					}
				}
			}
		}
		for(int  i=0;i<V;i++){
			System.out.println(distances[i]);
		}
	}

	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {
		// TODO Auto-generated method stub
		return DijkstraOrder;
	}

	@Override
	public boolean runBellmanFord(int src, int[] distances) {
		// TODO Auto-generated method stub
		for (int node = 0; node < V; node++) {
			distances[node] = Integer.MAX_VALUE / 2;
		}
		distances[src] = 0;
		for (int node = 0; node < V - 1; node++) {
			for (int sourcenode = 0; sourcenode < V; sourcenode++) {
				for (int i = 0; i < adjacencyList.get(sourcenode).size(); i++) {
					if (distances[adjacencyList.get(sourcenode).get(i).first] > distances[sourcenode]
							+ adjacencyList.get(sourcenode).get(i).second)
						distances[adjacencyList.get(sourcenode).get(i).first] = distances[sourcenode]
								+ adjacencyList.get(sourcenode).get(i).second;
				}
			}
		}
		for (int sourcenode = 0; sourcenode < V; sourcenode++) {
			for (int i = 0; i < adjacencyList.get(sourcenode).size(); i++) {
				if (distances[adjacencyList.get(sourcenode).get(i).first] > distances[sourcenode]
						+ adjacencyList.get(sourcenode).get(i).second) {
					return false;
				}

			}
		}
		
		for(int  i=0;i<V;i++){
			System.out.println(distances[i]);
		}
		return true;
	}

}
