package eg.edu.alexu.csd.filestructure.graphs;

import java.io.File;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Graph x =new Graph();
		File y =new File ("src/eg/edu/alexu/csd/filestructure/graphs/text1");
		x.readGraph(y);
		int []sx =new int [16];
		//for(int i=0;i<5;i++)
		//	sx[i]=Integer.MAX_VALUE;
		x.runDijkstra(0, sx);
		x.runBellmanFord(0, sx);
	}

}
/*9 27
0 1 4
0 7 8
1 0 4
1 2 8
1 7 11
2 1 8
2 3 7
2 5 4
2 8 2
3 2 7
3 4 9
3 5 14
4 3 9
4 5 10
5 2 4
5 4 10
5 6 2
6 3 14
6 5 2
6 7 1
6 8 6
7 0 8
7 1 11
7 6 1
7 8 7
8 2 2
8 6 6
8 7 7*/	

/*5 6
0 1 9
0 2 6
0 3 5
0 4 3
2 1 2
2 3 4
*/