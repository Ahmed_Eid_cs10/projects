package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Collection;

public class Sort<T extends Comparable<T>> implements ISort<T> {
	@Override
	
	//heap sort algorithm   O (nlgn )
	public IHeap<T> heapSort(ArrayList<T> unordered) {
		// TODO Auto-generated method stub
	
        IHeap<T> heap = new Heap<T>() ;
    	heap.build(unordered);
    	while(heap.size()>1){
    		heap.extract();
    	}
    	
    	((Heap<T>)heap).setSize(unordered.size());

		return heap;
		}
		
		
	//bubble sort  O (n ^2 )

	@Override
	public void sortSlow(ArrayList<T> unordered) {
		// TODO Auto-generated method stub
		
	        for (int i = 0; i < unordered.size()-1; i++)
	            for (int j = 0; j < unordered.size()-i-1; j++)
	                if (unordered.get(j).compareTo(unordered.get(j+1))>0)
	                {                    
	                    T temp = unordered.get(j);
	                    unordered.set(j, unordered.get(j+1));
	                    unordered.set(j+1, temp);
	                }
		
	           
	    
	}
	
	// Quick sort  O ( nlgn)

	@Override
	public void sortFast(ArrayList<T> unordered) {
		// TODO Auto-generated method stub
		QuickSort quickSort= new QuickSort();
		quickSort.sort(unordered, 0, unordered.size()-1);
		
		
	}
	
	

}