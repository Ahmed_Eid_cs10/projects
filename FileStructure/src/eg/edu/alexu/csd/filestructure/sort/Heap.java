package eg.edu.alexu.csd.filestructure.sort;


import java.util.ArrayList;
import java.util.Collection;


public class Heap<T extends Comparable<T>> implements IHeap<T> {
	private java.util.ArrayList<INode> heap =new ArrayList<>();
	private int size =0;
	@Override
	public INode<T> getRoot() {
		// TODO Auto-generated meth od stub

		if (size != 0) {
			return heap.get(0);
		} else {
			return null;
		}
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return this.size;
	}

	@Override
	public void heapify(INode<T> node) {
		// TODO Auto-generated method stub

		
		INode<T> left = node.getLeftChild();
		INode<T> right = node.getRightChild();
		INode<T> largest = new Node();

		if ((left != null) && left.getValue().compareTo(node.getValue()) > 0&&heap.indexOf(left)<size) {
			largest = left;
		} else {
			largest = node;
		}

		if ((right != null) && right.getValue().compareTo(largest.getValue()) > 0&&heap.indexOf(right)<size) {
			largest = right;
		}

		if (largest != null && largest.getValue().compareTo(node.getValue()) != 0&& heap.indexOf(largest)<size) {
			T temp = largest.getValue();
			largest.setValue(node.getValue());
			node.setValue( (T) temp);
			heapify(largest);
		}
	}

	@Override
	public T extract() {
		// TODO Auto-generated method stub
		if(heap.size()==0){
			return null;
		}else if(heap.size()==1){

			size--;
			return (T)heap.get(0).getValue();
			
		}else{
			T deleted = this.getRoot().getValue();
			heap.get(0).setValue(heap.get(size-1).getValue());
			heap.get(size-1).setValue(deleted);
			size--;
			this.heapify(heap.get(0));
			return deleted;
		}
	}

	@Override
	public void insert(T element) {

		INode<T> newNode;
		if(size==heap.size()){

			 newNode = new Node( element,size ) ;
			heap.add(size, newNode);
		}else{
			heap.get(size).setValue(element);
			 newNode=heap.get(size);
		}
			size++;

			INode<T> parent = newNode.getParent() ;
			while(parent!=null && element.compareTo(parent.getValue()) > 0 ) {
				newNode.setValue(parent.getValue());
				parent.setValue((T) element); 
				newNode = parent ;
				parent = parent.getParent() ;
			}
		
		
	
			
			
			
		
		
	}

	@Override
	public void build(Collection<T> unordered) {
		// TODO Auto-generated method stub
		for (T dummy : unordered) {
			heap.add(new Node(dummy,size++));
		}
		for (int i = (int) Math.floor(heap.size() / 2.0)-1; i >= 0; i--) {
			this.heapify(heap.get(i));
		}


	}

	

	
	
	void setSize(int value){
		size=value;
	}
	
	
	

	
	private class Node<A> implements INode<T> {
		private int index;
		private T element;
		
		public Node(){
			
		}

		public Node(T el,int x) {
			this.element = el;
			index=x;
		}

		@Override
		public INode<T> getLeftChild() {
			// TODO Auto-generated method stub
			int temp=((2 * Node.this.index )+1);
			if(temp>=Heap.this.size()){
				return null;
			}else{
				return (INode<T>) heap.get(temp);			
				
			}
			 
		}

		@Override
		public INode<T> getRightChild() {
			// TODO Auto-generated method stub
			int temp=(2 * Node.this.index) + 2;
			if(temp>=Heap.this.size()){
				return null;
			}else{
				return (INode<T>) heap.get(temp);
			}
			
		}

		@Override
		public INode<T> getParent() {
			// TODO Auto-generated method stub+
			int temp =(Node.this.index -1 ) / 2;
			if (Node.this.index == 0) {
				return null;
			}
			return (INode<T>) heap.get(temp);
			
		}

		@Override
		public T getValue() {
			// TODO Auto-generated method stub
			return (T) element;
		}

		@Override
		public void setValue(T value) {
			// TODO Auto-generated method stub
			element= value;
		}

	

	}

}