package eg.edu.alexu.csd.filestructure.sort;

public class QuickSort {
	
	public <T> void sort(java.util.ArrayList<T> A , int p , int r){
		
		if(p<r){
			int q=partition(A, p, r);
			sort(A, p, q-1);
			sort(A,q+1,r);
		}
		
	}
	
	private <T> int partition (java.util.ArrayList<T> A , int p , int r){
	    int x=  r;
		int i = p-1,j;
		for( j=p;j<=r-1;j++){
			if(((Comparable<T>) A.get(j)).compareTo(A.get(x)) <= 0){
				
				i++;
				T temp = A.get(i);
				A.set(i, A.get(j));
				A.set(j, temp);
				
				
			}
		}
		T temp = A.get(i+1);
		A.set(i+1, A.get(r));
		A.set(r, temp);
		return i+1;
		
		
	}
	

}
