package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Random;

public class Main {
public static void main(String[] args) {
		
		ArrayList list = new ArrayList();
		ISort sort =  new Sort () ;
		int size ;
		long startTime; 
		long stopTime;
		long elapsedTime ;
		 
		//generate random elements in arrayList
		Random randomGenerator = new Random();
		size = randomGenerator.nextInt(1000000) ;
		for (int idx = 0; idx <= size; ++idx){
		      list.add(randomGenerator.nextInt(100) );
		}
		
		 //test execution time for insertion sort
		  startTime = System.currentTimeMillis();
          sort.sortFast(list) ;
          
       //   sort.heapSort(list) ;
 	     
      //    sort.sortSlow(list) ;

          
          
	      stopTime = System.currentTimeMillis();
	      elapsedTime = stopTime - startTime;
	      System.out.println( "Quick execution time : " + (stopTime - startTime) ) ;
	      
		
		
		
      
	      
	      
	   }
		
}

