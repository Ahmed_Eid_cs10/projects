package eg.edu.alexu.csd.filestructure.avl;

public class Node<T extends Comparable<T>> implements INode<T> {
	private T valueOfNode;
	private int height;
	private Node left, right;

	Node(Comparable x) {
		this.height = 1;
		valueOfNode = (T) x;
	}

	public INode<T> getLeftChild() {
		// TODO Auto-generated method stub
		return left;
	}

	@Override
	public INode<T> getRightChild() {
		// TODO Auto-generated method stub
		return right;
	}

	@Override
	public T getValue() {
		// TODO Auto-generated method stub
		return valueOfNode;
	}

	@Override
	public void setValue(T value) {
		// TODO Auto-generated method stub
		valueOfNode = value;
	}

	public void setLeft(Node l) {
		left = l;
		// System.out.println(left);

	}

	public void setRight(Node r) {
		this.right = r;
		// System.out.println(right);
	}

	public int getHeight() {
		return height;

	}

	public void setHeight(int height) {
		this.height = height;
	}

}
