package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary implements IDictionary {
	private AVLTree<String> treeDictionary = new AVLTree<String>();

	@Override
	public void load(File file) {
		// TODO Auto-generated method stub

		FileReader fr = null;
		try {
			fr = new FileReader(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader br = new BufferedReader(fr);
		String line;
		try {
			while ((line = br.readLine()) != null) {
				this.insert(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean insert(String word) {
		// TODO Auto-generated method stub
		if (!treeDictionary.search(word)) {
			treeDictionary.insert(word);
			return true;
		}
		return false;
	}

	@Override
	public boolean exists(String word) {
		// TODO Auto-generated method stub
		return treeDictionary.search(word);
	}

	@Override
	public boolean delete(String word) {
		// TODO Auto-generated method stub
		return treeDictionary.delete(word);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return treeDictionary.getSize();
	}

	@Override
	public int height() {
		// TODO Auto-generated method stub
		return treeDictionary.height();
	}

}
