package eg.edu.alexu.csd.filestructure.avl;

public class AVLTree<T extends Comparable<T>> implements IAVLTree<T> {
	private Node<?> root;
	private boolean found;
	private int size;

	@Override
	// Function to insert in AVL TREE
	public void insert(Comparable key) {
		size++;
		root = insertRecursive(root, key); // CALL RECURSIVE INSERT

	}

	@Override
	// FUNCTION TO DELETE FROM AVL TREE
	public boolean delete(Comparable key) {

		found = false;
		root = deleteRecursive(root, key); // CALL RECURSIVE DELETE
		if (found) { // ELEMENT IS FOUND IN TREE
			size--;
		}
		return found ? true : false;

	}

	@Override
	// FUNCTION TO SEARCH IN AVL TREE
	public boolean search(Comparable key) {
		found = false;
		searchRecursive(root, key);
		return found ? true : false;
	}

	@Override
	// FUNCTION TO GET HEIGHT IN AVL TREE
	public int height() {
		// TODO Auto-generated method stub
		return getHeight(root);
	}

	@Override
	// FUNCTION TO GET THE ROOT OF THE TREE
	public INode getTree() {
		// TODO Auto-generated method stub
		return root;
	}

	private Node insertRecursive(Node node, Comparable key) {
		if (node == null) {
			return new Node(key);
		}
		if (node.getValue().compareTo((T) key) > 0) {
			node.setLeft(insertRecursive((Node) node.getLeftChild(), key));
		} else {
			node.setRight(insertRecursive((Node) node.getRightChild(), key));
		}
		// UPDATE NEW HEIGH
		node.setHeight(max(getHeight((Node) node.getLeftChild()), getHeight((Node) node.getRightChild())) + 1);
		// GET BALANCE FACTOR
		int balance = getBalance(node);

		// Left Left Case
		if (balance > 1 && key.compareTo(node.getLeftChild().getValue()) < 0) {
			return rightRotate(node);
		}
		// Left Right Case
		if (balance > 1 && key.compareTo(node.getLeftChild().getValue()) >= 0) {
			node.setLeft(leftRotate((Node) node.getLeftChild()));
			return rightRotate(node);
		}
		// Right Left Case
		if (balance < -1 && key.compareTo(node.getRightChild().getValue()) < 0) {
			node.setRight(rightRotate((Node) node.getRightChild()));
			return leftRotate(node);
		}
		// Right Right Case
		if (balance < -1 && key.compareTo(node.getRightChild().getValue()) >= 0) {
			return leftRotate(node);
		}

		return node;
	}

	// RECURSIVE DELETION
	private Node deleteRecursive(Node node, Comparable key) {

		if (node == null) {
			return node;
		}
		if (node.getValue().compareTo((T) key) > 0) {
			node.setLeft(deleteRecursive((Node) node.getLeftChild(), key));
		} else if (node.getValue().compareTo((T) key) < 0) {
			node.setRight(deleteRecursive((Node) node.getRightChild(), key));
		} else {
			found = true;
			Node temp = null;
			if (node.getLeftChild() == null && node.getRightChild() == null) {
				node = null;
			} else if (node.getLeftChild() == null) {
				node.setValue(node.getRightChild().getValue());
				node.setRight(null);
			} else if (node.getRightChild() == null) {
				node.setValue(node.getLeftChild().getValue());
				node.setLeft(null);
			} else {
				temp = getPredecessor((Node) node.getRightChild());
				node.setValue(temp.getValue());
				node.setRight(deleteRecursive((Node) node.getRightChild(), temp.getValue()));
			}
			if (node == null) {
				return node;
			}
		}

		node.setHeight(max(getHeight((Node) node.getLeftChild()), getHeight((Node) node.getRightChild())) + 1);

		int balance = getBalance(node);

		// Left Left Case

		if (balance > 1 && getBalance((Node) node.getLeftChild()) >= 0) {
			return rightRotate(node);
		}
		// Left Right Case
		if (balance > 1 && getBalance((Node) node.getLeftChild()) < 0) {
			node.setLeft(leftRotate((Node) node.getLeftChild()));
			return rightRotate(node);
		}
		// Right Left Case
		if (balance < -1 && getBalance((Node) node.getRightChild()) > 0) {
			node.setRight(rightRotate((Node) node.getRightChild()));
			return leftRotate(node);
		}
		// Right Right Case
		if (balance < -1 && getBalance((Node) node.getRightChild()) <= 0) {
			return leftRotate(node);
		}

		return node;

	}

	private void searchRecursive(Node node, Comparable key) {
		if (node == null) {
			return;
		}
		if (node.getValue().compareTo((T) key) < 0) {
			searchRecursive((Node) node.getRightChild(), key);
		} else if (node.getValue().compareTo((T) key) > 0) {
			searchRecursive((Node) node.getLeftChild(), key);
		} else {
			found = true;
		}
	}

	private int getHeight(Node node) {
		if (node == null) {
			return 0;
		}
		return (node).getHeight();
	}

	private int max(int a, int b) {
		return (a > b) ? a : b;
	}

	private int getBalance(Node node) {
		if (node == null) {
			return 0;
		}
		return getHeight((Node) node.getLeftChild()) - getHeight((Node) node.getRightChild());
	}

	private Node leftRotate(Node node) {
		Node x = (Node) node.getRightChild();
		Node y = null;
		if (x != null) {
			y = (Node) x.getLeftChild();

			x.setLeft(node);
			node.setRight(y);

			node.setHeight(max(getHeight((Node) node.getLeftChild()), getHeight((Node) node.getRightChild())) + 1);
			x.setHeight(max(getHeight((Node) x.getLeftChild()), getHeight((Node) x.getRightChild())) + 1);
			return x;
		} else {
			return node;
		}

	}

	private Node rightRotate(Node node) {
		Node x = (Node) node.getLeftChild();
		Node y = (Node) x.getRightChild();

		x.setRight(node);
		node.setLeft(y);

		node.setHeight(max(getHeight((Node) node.getLeftChild()), getHeight((Node) node.getRightChild())) + 1);
		x.setHeight(max(getHeight((Node) x.getLeftChild()), getHeight((Node) x.getRightChild())) + 1);
		return x;

	}

	// GET predecessor of node in AVL TREE

	private Node getPredecessor(Node node) {
		Node currentNode = node;
		while (currentNode.getLeftChild() != null) {
			currentNode = (Node) currentNode.getLeftChild();
		}
		return currentNode;

	}

	// get size of AVL TREE
	public int getSize() {
		return this.size;
	}

}
