package eg.edu.alexu.csd.filestructure.hash;

import java.util.LinkedList;

public class HashLinear<K, V> implements IHash<K, V>, IHashLinearProbing{
	private Pair<K,V>[] hashTable =new Pair [1200];
	private Pair<K,V>[] temp;
	private boolean [] visited = new boolean [1200];
	private int collisions=0,size=0,hashTableSize=1200, secret=0; 
	@Override
	public void put(K key, V value) {
		// TODO Auto-generated method stub
		int i=0;
		if(size<hashTableSize){
			size++;
			int k = key.hashCode() % hashTableSize;
			while(true){		
				if(hashTable[k]==null){
					hashTable[k]=new Pair(key,value);
					visited[k]=true;
					break;
				}
				i++;
				k=(k+1)%hashTableSize;
				collisions++;
			}
			if(i!=0){
				collisions++;
			}
		}else{
			collisions++;
			size=0;
			collisions+=hashTableSize;
			temp=new Pair[hashTableSize];
			for(int j=0;j<hashTableSize;j++){
				temp[j]=hashTable[j];
			}
			hashTableSize=hashTableSize*2;
			hashTable=new Pair[hashTableSize];
			visited=new boolean [hashTableSize];
			for(int j=0;j<temp.length;j++){
				put(temp[j].getKey(),temp[j].getValue());
			}
			put(key,value);
		}
	}

	@Override
	public String get(K key) {
		// TODO Auto-generated method stub
		int k = key.hashCode() % hashTableSize;
		int i=0;
		while (i<hashTableSize){
			i++;
			if(hashTable[k]!=null||visited[k]){
				if( hashTable[k]!=null && key.equals(hashTable[k].getKey()) ){
					return (String) hashTable[k].getValue();
				}else{
					k=(k+1)%hashTableSize;
				}
			}else{
				return null;
			}

		}
		return null;
	}

	@Override
	public void delete(K key) {
		// TODO Auto-generated method stub
		int k = key.hashCode() % hashTableSize;
		int i=0;
		while (i<hashTableSize){
			i++;
			if(hashTable[k]!=null||visited[k]){
				if( hashTable[k]!=null && key.equals(hashTable[k].getKey() )){
					hashTable[k]=null;
					size--;
					break;
				}else{
					k=(k+1)%hashTableSize;
				}
			}else{
				break;
			}
		}

	}
	
	

	@Override
	public boolean contains(K key) {
		// TODO Auto-generated method stub
		int k = key.hashCode() % hashTableSize;
		while (true){
			if(hashTable[k]!=null||visited[k]){
				if( hashTable[k]!=null && key .equals(hashTable[k].getKey() )){
					return true;
				}else{
					k=(k+1)%hashTableSize;
				}
			}else{
				return false;
			}
			
			
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0?true:false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return hashTableSize;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub

		return collisions;
	}

	@Override
	public Iterable<K> keys() {
		// TODO Auto-generated method stub
		LinkedList<K> returnList = new LinkedList<K>();
		for(int i=0;i<hashTableSize;i++){
			if(hashTable[i]!=null){
				returnList.add((K) hashTable[i].getKey());
			}
		}
		return returnList;
	}
}
