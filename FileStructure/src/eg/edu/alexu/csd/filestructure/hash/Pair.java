	package eg.edu.alexu.csd.filestructure.hash;
public class Pair<K,V> {
	
	private K key;
	private V value ;
	public Pair(K a, V b) {
		// TODO Auto-generated constructor stub
		key=a;
		value=b;
	}
	
	public K getKey() {
		return key;
	}
	public void setKey(K key) {
		this.key = key;
	}
	public V getValue() {
		return value;
	}
	public void setValue(V value) {
		this.value = value;
	}
}