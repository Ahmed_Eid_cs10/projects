package eg.edu.alexu.csd.filestructure.hash;
import java.util.LinkedList;

public class HashChaining<K, V> implements IHash<K, V>, IHashChaining{

	private LinkedList<Pair>[] hashTable = new LinkedList[1200];
	private int collisions =0;
	HashChaining() {
		for (int i = 0; i < hashTable.length; i++) {
			hashTable[i] = new LinkedList<Pair>();
		}
	}

	public void put(K key, V value) {
		int k = key.hashCode() % 1200;
		collisions += hashTable[k].size();
		hashTable[k].add(new Pair(key, value));
	}
	@Override
	public String get(K key) {
		int k = key.hashCode() % 1200;
		for (int i = 0; i < hashTable[k].size(); i++) {
			if (hashTable[k].get(i).getKey().equals(key)) {
				return (String) hashTable[k].get(i).getValue();
			}
		}
		return null;
	}

	@Override
	public void delete(K key) {
		int k = key.hashCode() % 1200;
		for (int i = 0; i < hashTable[k].size(); i++) {
			if (hashTable[k].get(i).getKey().equals(key)) {
				hashTable[k].remove(i);
			}
		}
	}
	@Override
	public boolean contains(K key) {
		// TODO Auto-generated method stub
		
		int k = key.hashCode() % 1200;
		for (int i = 0; i < hashTable[k].size(); i++) {
			if (hashTable[k].get(i).getKey().equals(key)) {
					return true;
			}
		}
		return false;
	}

	public boolean isEmpty() {
		for (int i = 0; i < hashTable.length; i++) {
			if (hashTable[i].size() != 0) {
				return false;
			}
		}
		return true;

	}
	@Override
	public int size() {
		// TODO Auto-generated method stub
		int size = 0;
		for (int i = 0; i < hashTable.length; i++) {
			size+=hashTable[i].size();
		}
		return size;
	}
	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return 1200;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub
		return collisions;
	}

	@Override
	public Iterable<K> keys() {
		// TODO Auto-generated method stub
		LinkedList<K> returnList = new LinkedList<K>();
		for (int i = 0; i < hashTable.length; i++) {
			for (Pair<K, V> p : hashTable[i]) {
				returnList.add(p.getKey());
			}
		}
		return returnList;
	}

}