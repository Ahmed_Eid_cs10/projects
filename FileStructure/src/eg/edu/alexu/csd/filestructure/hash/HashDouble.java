package eg.edu.alexu.csd.filestructure.hash;

import java.util.LinkedList;

public class HashDouble<K, V> implements IHash<K, V>, IHashDouble{
	private Pair [] hashTable =new Pair [1200];
	private Pair<K,V>[] temp;
	private boolean [] visited = new boolean [1200];
	private int collisions=0,size=0,hashTableSize=1200,prime=1193; 
	private boolean flag2=false;
	
	@Override
public void put(K key, V value) {
		// TODO Auto-generated method stub
			int h1 = key.hashCode() % hashTableSize;
			int h2=prime-((Integer)key%prime);
			int flag=0;
			size++;
				while(true){	
					int k=(h1+flag*h2)%hashTableSize;
					if(hashTable[k]==null){
						hashTable[k]=new Pair(key,value);
						visited[k]=true;
						break;
					}
					collisions++;
					flag++;
					if(flag==hashTableSize){
						collisions++;
						size=0;
						temp=new Pair[hashTableSize];
						for(int i=0;i<hashTableSize;i++){
							temp[i]=hashTable[i];
						}
						hashTableSize=hashTableSize*2;
						hashTable=new Pair[hashTableSize];
						visited=new boolean [hashTableSize];
						for(int i=0;i<temp.length;i++){
							if(temp[i]!=null)
								put(temp[i].getKey(),temp[i].getValue());
						}
						put(key,value);
						break;
						
					}
					
				}
	}
	@Override
	public String get(K key) {
		// TODO Auto-generated method stub
		int h1 = key.hashCode() % hashTableSize;
		int h2=prime-(Integer)key%prime;
		int flag=0;
		int k=(h1+flag++*h2)%hashTableSize;
		while (flag<hashTableSize){
			if(hashTable[k]!=null||visited[k]){
				if( hashTable[k]!=null && key.equals( hashTable[k].getKey() )){
					return (String) hashTable[k].getValue();
				}else{
					 k=(h1+flag++*h2)%hashTableSize;
				}
			}else{
				return null;
			}
		}
		return null;
	}

	@Override
	public void delete(K key) {
		// TODO Auto-generated method stub
		int h1 = key.hashCode() % hashTableSize;
		int h2=prime-(Integer)key%prime;
		int i=0;
		int k=(h1+i++*h2)%hashTableSize;
		while (i<hashTableSize){
			if(hashTable[k]!=null||visited[k]){
				if( hashTable[k]!=null && key .equals( hashTable[k].getKey()) ){
					hashTable[k]=null;
					size--;
					break;
				}else{
					 k=(h1+i++*h2)%hashTableSize;
				}
			}else{
				break;
			}
		}

	}


	@Override
	public boolean contains(K key) {
		// TODO Auto-generated method stub
		int h1 = key.hashCode() % hashTableSize;
		int h2 = prime - (Integer)key  %  prime;
		int k = (h1) % hashTableSize;
		int i = 0;
		while(i < hashTableSize ) {
			i++;
			if(hashTable[k] != null || visited[k] == true) {
				if(hashTable[k]!=null && key.equals(hashTable[k].getKey())) {
					return true;
				}else {
					k = (h1 + i * h2) % hashTableSize;
				}
			}else {
				return false;
			}
		}
		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0?true:false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return hashTableSize;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub
		return collisions;
	}

	@Override
	public Iterable<K> keys() {
		// TODO Auto-generated method stub
		LinkedList<K> returnList = new LinkedList<K>();
		for(int i=0;i<hashTableSize;i++){
			if(hashTable[i]!=null){
				returnList.add((K) hashTable[i].getKey());
			}
		}
		return returnList;
	}

	
	
}
