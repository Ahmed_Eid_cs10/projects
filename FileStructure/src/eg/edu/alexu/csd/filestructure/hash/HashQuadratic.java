package eg.edu.alexu.csd.filestructure.hash;

import java.util.LinkedList;

public class HashQuadratic<K, V> implements IHash<K, V>, IHashQuadraticProbing{
	
	private int hashTableSize = 1200;
	private Pair<K,V>[] hashTable = new Pair[1200];
	private Pair<K,V>[] temp;
	private boolean[] visited = new boolean[1200];
	private int collisions = 0, size = 0;
	@Override
	public void put(K key, V value) {
		// TODO Auto-generated method stub
		
			int hashedKey = key.hashCode() % hashTableSize;
			int flag = 0;
			size++;
			while (true) {
				int k = (hashedKey + flag*flag) % hashTableSize;
				
				if (hashTable[k] == null) {
					hashTable[k] = new Pair<K,V>(key, value);
					visited[k] = true;
					break;
				}
				flag++;
				collisions++;
				if(flag==hashTableSize) {
					size = 0 ;
					temp = new Pair[hashTableSize];
					for(int i = 0 ; i < hashTable.length; i++) {
						temp[i] = hashTable[i];
					}
					hashTableSize *= 2;
					hashTable = new Pair[hashTableSize];
					visited = new boolean[hashTableSize];
					for(int i = 0 ; i < hashTableSize/2 ; i++) {
						if(temp[i]!=null)
							put(temp[i].getKey(), temp[i].getValue());
					}
					put(key,value);
					break;
				}
			}
			if(flag!=0){
				collisions++;
			}
	}

	@Override
	public String get(K key) {
		// TODO Auto-generated method stub
		int hashedKey = key.hashCode() % hashTableSize;
		int k = hashedKey;
		
		int i = 0;
		while (i < hashTableSize) {
			
			i++;
			if (visited[k]) {
				if (hashTable[k] != null && key.equals(hashTable[k].getKey())) {
					return (String) hashTable[k].getValue();
				} else {
					k = (hashedKey + i * i) % hashTableSize;
				}
			} else {
				
				return null;
			}
		}
		return null;
	}

	@Override
	public void delete(K key) {
		// TODO Auto-generated method stub
		int hashedKey = key.hashCode() % hashTableSize;
		int k = hashedKey;
		
		int i = 0;
		while (i < hashTableSize) {
			i++;
			if ( visited[k] ) {
				if (hashTable[k] != null && key.equals(hashTable[k].getKey())) {
					hashTable[k] = null;
					size--;
					break;
				} else {
					k = (k + i*i) % hashTableSize;
				}
			} else {
				break;
			}
		}

	}

	@Override
	public boolean contains(K key) {
		// TODO Auto-generated method stub
		int k = key.hashCode() % hashTableSize;
		int i = 0;
		while (true) {
			i++;
			if (hashTable[k] != null || visited[k]) {
				if (hashTable[k] != null && key.equals(hashTable[k].getKey())) {
					return true;
				} else {
					k = (k + i*i) % hashTableSize;
				}
			} else {
				return false;
			}

		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return size==0?true:false;
		
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return hashTableSize;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub
		return collisions;
	}


	@Override
	public Iterable<K> keys() {
		// TODO Auto-generated method stub
		LinkedList<K> returenList = new LinkedList<K>();
		for(int i=0;i<hashTableSize;i++){
			if(hashTable[i]!=null){
				returenList.add((K) hashTable[i].getKey());
			}
		}
		return returenList;
	}


}
